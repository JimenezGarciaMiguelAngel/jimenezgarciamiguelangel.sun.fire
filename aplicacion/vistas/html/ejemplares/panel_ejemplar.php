<?php foreach ($datos['ejemplares'] as $ejemplar) { ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		ejemplar: <strong><?php echo $ejemplar['observaciones_ejemplar']; ?></strong>
	</div>
	<div class="panel-body">
		<ul>
		 
		<li><strong>ISBN</strong> <?php echo $ejemplar['isbn']; ?></li>
		<li><strong>OBSERVACIONES</strong> <?php echo $ejemplar['observaciones_ejemplar']; ?></li>
		
		</ul>
	</div>

	<div class="panel-footer clearfix">
		<div class="pull-right">
			<a href="opcion.php?c=ejemplares&a=editar_ejemplar&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>" class="btn btn-default">Editar</a>
			<a href="opcion.php?c=ejemplares&a=borrar_ejemplar&id_ejemplar=<?php echo $ejemplar['id_ejemplar']; ?>" class="btn btn-warning">Borrar</a>
		</div>
	</div>
</div>
<?php } ?>