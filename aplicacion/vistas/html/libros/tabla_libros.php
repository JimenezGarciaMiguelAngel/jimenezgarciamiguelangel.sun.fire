<div class="row">
	<div class="col-md-2">
		<a href="opcion.php?c=libros&a=ver_lista&v=excel"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a Excel
		</a>
	</div>
	<div class="col-md-2">
		<a href="opcion.php?c=libros&a=ver_lista&v=pdf"
			class="btn btn-success btn-xs boton_exportar"> <span
			class="glyphicon glyphicon-export"></span> Exportar a PDF
		</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-condensed">
			<caption>Libros</caption>
			<thead>
				<tr>
					<th>Libro</th>
					<th>Opciones</th>
				</tr>
			</thead>
			<tbody>
  <?php foreach ($datos['libros'] as $libro) { ?>
    <tr>
					<td><?php echo $libro['titulo_libro']; ?></td>
					<td><a
						href="opcion.php?c=libros&a=ver_libro&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-primary btn-xs">Información</a> <a
						href="opcion.php?c=libros&a=editar_libro&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-default btn-xs">Editar</a> <a
						href="opcion.php?c=libros&a=borrar_libro&id_libro=<?php echo $libro['id_libro']; ?>"
						class="btn btn-warning btn-xs">Borrar</a></td>
				</tr>
  <?php } ?>
    </tbody>
		</table>
	</div>
</div>