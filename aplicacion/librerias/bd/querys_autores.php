<?php

/* Requerimos de acceso a la base de datos */
require_once "aplicacion/librerias/bd/base_datos.php";

function select_autores()
{
    /* Obtenemos una conexión a la base de datos */
    $bd = obtener_conexion_base_datos();
    /* Si durante la conexión se presentó algún error,
     * lo "notificamos" al modelo que nos haya llamado.
     */
    if ($bd['error'] == true) {
        return $bd;
    }
    
    $query = "
        select
            id_autor,
            nombre_autor,
            nacionalidad_autor
        from
            autores
        order by
            nombre_autor";
    
    /* Ejecutamos la consulta, sobre la conexión abierta a
     * la base de datos
     */
    $consulta = pg_query_params($bd['conexion'], $query, array());

    /* Antes de regresar los datos o el *posible error de consulta*,
     * cerramos la conexión a la base de datos.
    */
    cerrar_conexion_base_datos($bd['conexion']);
    
    /* Si se presentó algún error durante la ejecución de
     * la consulta... aún cuando sí exista una conexión
     * a la base de datos... se lo "notificamos" al
     * modelo que nos haya llamado.
     */
    if ($consulta == false) {
        return array(
            'error' => true,
            'mensajes_error' => array(
                'No se ha podido obtener información de los autores.'
            )
        );
    }
    
    /* Y finalmente regresamos los datos */
    return array(
        'error' => false,
        'datos' => pg_fetch_all($consulta)
    );
}