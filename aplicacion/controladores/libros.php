<?php

/* Controlador */
function ver_lista($aplicacion)
{
    $datos = array();
    
    /*
     * Determinamos si nos están solicitando algún "formato" en particular.
     */
    $tipo_vista = @$_GET['v'];
    
    /*
     * Si no podemos generar el tipo de "formato" solicitado por el cliente no tiene sentido que usemos un modelo, ni mucho menos que él acceda a su fuente de datos.
     */
    if (empty($tipo_vista) || ($tipo_vista != "panel" && $tipo_vista != "tabla" && $tipo_vista != "excel" && $tipo_vista != "pdf")) {
        $datos['mensajes_error'][] = 'El tipo de vista solicitada no es reconocido.';
        $datos['vista']['titulo'] = 'libros - Lista de libros - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    
    /*
     * Hasta este punto, sabemos que sí podremos generar el formtato que el cliente nos solicita.
     */
    
    /* Requerimos de un modelo para *obtener a los libros*.
     */
    require "aplicacion/modelos/libros.php";
    $resultado = obtener_libros($aplicacion);
    
    /*
     * Si el modelo me indica que existió algún error... cualquier error, se lo indicamos al *cliente*.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'libros - Lista de libros - Error';
        /*
         * Existen errores que son *fatales* y se presentan en cualquier punto de la aplicación, por lo tanto utilizo una vista general para los errores.
         */
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    
    $datos['libros'] = $resultado['datos'];
    $datos['vista']['titulo'] = 'libros - Lista de libros';
    
    /* HTML usando paneles */
    if ($tipo_vista == "panel") {
        $datos['vista']['cuerpo'] = 'html/libros/panel_libros.php';
        require "aplicacion/vistas/html/base/base.php";
        return true;
    }
    
    /* HTML usando una tabla */
    if ($tipo_vista == "tabla") {
        $datos['vista']['cuerpo'] = 'html/libros/tabla_libros.php';
        require "aplicacion/vistas/html/base/base.php";
        return true;
    }
    
    /* Excel */
    if ($tipo_vista == "excel") {
        require "aplicacion/vistas/excel/libros/libros_full.php";
        return true;
    }
    
    /* PDF */
    if ($tipo_vista == "pdf") {
        require "aplicacion/vistas/pdf/libros/libros_full.php";
        return true;
    }
}

function ver_libro($aplicacion)
{
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/libros.php";
    
    /*
     * Obtenemos el *id del libro* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_libro = @$_GET['id_libro'];
    
    /* Y le solicitamos al modelo a *ese libro* en particular */
    $resultado = obtener_libro($aplicacion, $id_libro);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'libros - Ver libro - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del libro */
        $datos['libros'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'libros - Información de libro';
        $datos['vista']['cuerpo'] = 'html/libros/panel_libro.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}

function borrar_libro($aplicacion)
{
    $datos = array();
    $id_libro = @$_GET['id_libro'];
    require "aplicacion/modelos/libros.php";
    
    $resultado = eliminar_libro($aplicacion, $id_libro);
    
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Libros - Eliminar libro - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
    $datos['vista']['titulo'] = "Libros - Eliminado";
    $datos['vista']['cuerpo'] = 'html/libros/exito_eliminado_libro.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}

function nuevo_libro($aplicacion)
{
    $datos = array();
    $datos['vista']['titulo'] = "libros - Nuevo libro";
    $datos['vista']['cuerpo'] = 'html/libros/nuevo_libro.php';
    require "aplicacion/vistas/html/base/base.php";
}

function guardar_libro($aplicacion)
{
    $libro = @$_POST['libro'];
    require "aplicacion/modelos/libros.php";
    
    $resultado = guardar_datos_libro($aplicacion, $libro);
    
    if ($resultado['error'] == true) {
        $datos = $resultado;
        $datos['vista']['titulo'] = "libros - Nuevo libro";
        $datos['vista']['cuerpo'] = 'html/libros/nuevo_libro.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    $datos['libro'] = $resultado['datos'];
    $datos['vista']['titulo'] = "libros - Nuevo libro";
    $datos['vista']['cuerpo'] = 'html/libros/exito_nuevo_libro.php';
    require "aplicacion/vistas/html/base/base.php";
}

function editar_libro($aplicacion)
{
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/libros.php";
    
    /*
     * Obtenemos el *id del libro* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_libro = @$_GET['id_libro'];
    
    /* Y le solicitamos al modelo a *ese libro* en particular */
    $resultado = obtener_libro($aplicacion, $id_libro);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Libros - Editar libro - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del libro */
        $datos['libros'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Libros - Editar de libro';
        $datos['vista']['cuerpo'] = 'html/libros/modificar_libro.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}

function modificar_libro($aplicacion)
{
    $datos = array();
    $libro = @$_POST['libro'];
    $id_libro = @$_GET['id_libro'];
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/libros.php";
    
    
    /* Y le solicitamos al modelo a *ese libro* en particular */
    $resultado = modificar_datos_libro($aplicacion, $id_libro, $libro );
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Libro - Editar libro - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del libro */
        $datos['libro'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Libro - Editar de libro';
        $datos['vista']['cuerpo'] = 'html/libros/exito_modificado_libro.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}