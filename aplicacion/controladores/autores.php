<?php

/* Controlador */
function ver_lista($aplicacion)
{
    $datos = array();
    
    /*
     * Determinamos si nos están solicitando algún "formato" en particular.
     */
    $tipo_vista = @$_GET['v'];
    
    /*
     * Si no podemos generar el tipo de "formato" solicitado por el cliente no tiene sentido que usemos un modelo, ni mucho menos que él acceda a su fuente de datos.
     */
    if (empty($tipo_vista) || ($tipo_vista != "panel" && $tipo_vista != "tabla" && $tipo_vista != "excel" && $tipo_vista != "pdf")) {
        $datos['mensajes_error'][] = 'El tipo de vista solicitada no es reconocido.';
        $datos['vista']['titulo'] = 'Autores - Lista de autores - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    
    /*
     * Hasta este punto, sabemos que sí podremos generar el formtato que el cliente nos solicita.
     */
    
    /* Requerimos de un modelo para *obtener a los autores*.
     */
    require "aplicacion/modelos/autores.php";
    $resultado = obtener_autores($aplicacion);
    
    /*
     * Si el modelo me indica que existió algún error... cualquier error, se lo indicamos al *cliente*.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Autores - Lista de autores - Error';
        /*
         * Existen errores que son *fatales* y se presentan en cualquier punto de la aplicación, por lo tanto utilizo una vista general para los errores.
         */
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    
    $datos['autores'] = $resultado['datos'];
    $datos['vista']['titulo'] = 'Autores - Lista de autores';
    
    /* HTML usando paneles */
    if ($tipo_vista == "panel") {
        $datos['vista']['cuerpo'] = 'html/autores/panel_autores.php';
        require "aplicacion/vistas/html/base/base.php";
        return true;
    }
    
    /* HTML usando una tabla */
    if ($tipo_vista == "tabla") {
        $datos['vista']['cuerpo'] = 'html/autores/tabla_autores.php';
        require "aplicacion/vistas/html/base/base.php";
        return true;
    }
    
    /* Excel */
    if ($tipo_vista == "excel") {
        require "aplicacion/vistas/excel/autores/autores_full.php";
        return true;
    }
    
    /* PDF */
    if ($tipo_vista == "pdf") {
        require "aplicacion/vistas/pdf/autores/autores_full.php";
        return true;
    }
}

function ver_autor($aplicacion)
{
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/autores.php";
    
    /*
     * Obtenemos el *id del autor* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_autor = @$_GET['id_autor'];
    
    /* Y le solicitamos al modelo a *ese autor* en particular */
    $resultado = obtener_autor($aplicacion, $id_autor);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Autores - Ver autor - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['autores'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Autores - Información de autor';
        $datos['vista']['cuerpo'] = 'html/autores/panel_autor.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}

function editar_autor($aplicacion)
{
    $datos = array();
    
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/autores.php";
    
    /*
     * Obtenemos el *id del autor* que el cliente nos está pasando como parámetro por GET. Cabe notar que no existe validación alguna... es el modelo que se encargará de eso.
     */
    $id_autor = @$_GET['id_autor'];
    
    /* Y le solicitamos al modelo a *ese autor* en particular */
    $resultado = obtener_autor($aplicacion, $id_autor);
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Autores - Editar autor - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['autores'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Autores - Editar de autor';
        $datos['vista']['cuerpo'] = 'html/autores/modificar_autor.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}

function borrar_autor($aplicacion)
{
    $datos = array();
    $id_autor = @$_GET['id_autor'];
    require "aplicacion/modelos/autores.php";
    
    $resultado = eliminar_autor($aplicacion, $id_autor);
    
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Autores - Eliminar autor - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
    $datos['vista']['titulo'] = "Autores - Eliminado";
    $datos['vista']['cuerpo'] = 'html/autores/exito_eliminado_autor.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}

function nuevo_autor($aplicacion)
{
    $datos = array();
    $datos['vista']['titulo'] = "Autores - Nuevo autor";
    $datos['vista']['cuerpo'] = 'html/autores/nuevo_autor.php';
    require "aplicacion/vistas/html/base/base.php";
}

function guardar_autor($aplicacion)
{
    $autor = @$_POST['autor'];
    require "aplicacion/modelos/autores.php";
    
    $resultado = guardar_datos_autor($aplicacion, $autor);
    
    if ($resultado['error'] == true) {
        $datos = $resultado;
        $datos['vista']['titulo'] = "Autores - Nuevo autor";
        $datos['vista']['cuerpo'] = 'html/autores/nuevo_autor.php';
        require "aplicacion/vistas/html/base/base.php";
        return false;
    }
    $datos['autor'] = $resultado['datos'];
    $datos['vista']['titulo'] = "Autores - Nuevo autor";
    $datos['vista']['cuerpo'] = 'html/autores/exito_nuevo_autor.php';
    require "aplicacion/vistas/html/base/base.php";
}

function modificar_autor($aplicacion)
{
    $datos = array();
    $autor = @$_POST['autor'];
    $id_autor = @$_GET['id_autor'];
    /* Requerimos acceso a un modelo */
    require "aplicacion/modelos/autores.php";
    
    
    /* Y le solicitamos al modelo a *ese autor* en particular */
    $resultado = modificar_datos_autor($aplicacion, $id_autor, $autor );
    
    /*
     * Si el modelo me indica que existio algún error, cualquiera, se lo indicamos al cliente.
     */
    if ($resultado['error'] == true) {
        $datos['mensajes_error'] = $resultado['mensajes_error'];
        $datos['vista']['titulo'] = 'Autores - Editar autor - Error';
        $datos['vista']['cuerpo'] = 'html/base/errores.php';
    } else {
        /* En caso contrario, mostramos la información del autor */
        $datos['autores'] = $resultado['datos'];
        $datos['vista']['titulo'] = 'Autores - Editar de autor';
        $datos['vista']['cuerpo'] = 'html/autores/exito_modificado_autor.php';
    }
    require "aplicacion/vistas/html/base/base.php";
}